// Write code for factorial even numbers
package SimpleCode;

class FactDemo {
	static void Fact(int num){
        int num1 = 0;
        
        while(num != 0){
            int rem = num%10;
            num1 = num1*10+rem;
            num = num/10;
        } 
        while(num1 != 0){
            int rem = num1%10;
            int mul = 1;
            if(rem % 2 == 0){ 
                for(int i = 1;i<=rem;i++){ 
                    mul = mul*i;
                }
                System.out.print(mul+" , ");
            }
            
            num1 = num1/10;
        }
    }

    public static void main(String[] args){
        int n = 256946;
        Fact(n);
        
    }

} 