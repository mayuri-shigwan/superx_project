/*WAP to count the size of given string
 * 
 */
package Assignment_3;

class StringCount{    
    public static void main(String[] args) {    
        String str = "Wellcome to Core2web";    
        int count = 0;    
                
        for(int i = 0; i < str.length(); i++) {    
            if(str.charAt(i) != ' ') {    
                count++;   
            } 
        }   
        System.out.println("Total Number of Charactor: " + count);    
    }    
}   