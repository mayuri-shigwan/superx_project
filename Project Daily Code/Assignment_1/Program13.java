/*5
 *6 8
 *7 10 13
 *8 12 16 20
 *9 14 19 24 29
 */
package SimpleCode;

class PatternDemo3 {
	public static void main(String[] args) {
		int num=5;
		for(int i=1; i<=5; i++) {
			int num1=num;
			for(int j=1; j<=i; j++) {
				
				System.out.print(num1+" ");
				num1=num1+i;
			}
			num++;
			System.out.println();
		}
		
	}
}
