/*     1 
     2 4 
   3 6 9 
4 8 12 16 
 * 
 */
package SimpleCode;

class PatternDemo2 {
	public static void main(String[] args) {
		int num=4;
		for(int i=1; i<=4; i++) {
			int num1=1;
			for(int j=1; j<=4; j++) {
				if(j<=4-i) {
					System.out.print("  ");
				}else {
					System.out.print(i*num1+" ");
					num1++;
				}
			}
			
			System.out.println();
		}
	}

}
