/*WAP to take string from user and convert all even indexes of string to uppercase and
 odd indexes of a string to lower case. 
 Input : dfTbnSrOvryt
 Output : DfTbNsRoVrY 
 */
package SimpleCode;

class upCase {
	public static void main(String[] args) {
		String str = "dfTbnSrOvryt";
        char ch[] =str.toCharArray();
        for(int i = 0;i<ch.length;i++){
            if(i%2 == 0){
                ch[i] = Character.toUpperCase(ch[i]);
            }else{
                ch[i] = Character.toLowerCase(ch[i]);;
            }
        }
        for(int j = 0;j<ch.length;j++){
            System.out.print(ch[j] + " ");
        }

	}
}
