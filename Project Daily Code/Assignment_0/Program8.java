/*WAP to find the occurrence of vowels in a given string. 
Input : adgtioseobi
Output : a = 1
e = 1
i = 2
o = 2
*/
package SimpleCode;

class VowelStr {
	public static void main(String[] args) {
		String str = "adgtioseobi";
        char ch1[] = str.toCharArray();
        int sum1 = 0;
        int sum2 = 0;
        int sum3 = 0;
        int sum4 = 0;
  

        for(int i = 0;i<ch1.length;i++){
            if(ch1[i] == 'a'){
                sum1++;
            }else if(ch1[i] == 'e'){
                sum2++;
            }else if(ch1[i] == 'i'){
                sum3++;
            }else if(ch1[i] == 'o'){
                sum4++;
            }
        }
        System.out.println("a = "+sum1);
        System.out.println("e = "+sum2);
        System.out.println("i = "+sum3);
        System.out.println("o = "+sum4);
	}
}
