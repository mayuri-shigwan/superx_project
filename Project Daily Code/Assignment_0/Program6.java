/*Write a print to find a number which has number  on it's left is less than or equal to
 *itself 
 * Input: 4 5 6 9 7 5 9 6 2 
*/
package SimpleCode;

class minNums{
	public static void main(String[] args) {
		int num = 459975962;
        int maxVal = Integer.MIN_VALUE;
        while(num != 0){
            int rem = num%10;
            if(maxVal < rem){
                maxVal = rem;
            }
            num = num/10;
        }
        System.out.println(maxVal);
	}

}
