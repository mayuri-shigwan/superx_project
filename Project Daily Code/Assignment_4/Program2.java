/*Take input form user
 * 1
 * 2 4
 * 3 6 9
 * 4 8 12 16
 *
 */
package Assignment_4;
class PatternProg {
	public static void main(String[] args) {
		int num = 4;
		for(int i=1;i<=num;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(i*j+" ");
				
			}
			System.out.println();
		}
	}

}
