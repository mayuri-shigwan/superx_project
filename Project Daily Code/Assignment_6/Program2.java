/*A
 *B A 
 *C B A
 *D C B A
 */
package Assignment_1;

class PatternCode2{
	public static void main(String[] args) {
		int num =4;
		for(int i =1;i<=num;i++) {
			for(int j =i;j>=1;j--) {
				System.out.print((char)(j+64)+" ");
			}
			System.out.println();
		}
	}
}
