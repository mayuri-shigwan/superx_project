/*Take the size of the array from the user. Create two arrays of that size. Initialize all
 second array elements as zero(0). For the first array take all elements from the user.
 Check if the elements in the first array are even or not if it's even then replace the
 value of the second array of that index with 1 and print both the array.
 Input : Size : 10
 Array 1 Elements : 4 2 3 6 8 7 1 0 9 5
 Output : Array 1 Elements : 4 2 3 6 8 7 1 0 9 5
 Array 2 Elements : 1 1 0 1 1 0 0 1 0 0
 */
package SimpleCode;

class ArrayDemo1 {
	public static void main(String[] args) {
		int arr[] = new int[]{4,2,3,6,8,7,1,0,9,5};
		int N = 10;
        int arr1[] = new int[N];

        for(int i = 0;i<N;i++){
            if(arr[i] % 2 == 0){
                arr1[i] = 1;
            }else{
                arr1[i] = 0;
            }
        }
        for(int i = 0;i<N;i++){
            System.out.print(arr1[i] + " ");
        }
	}
}
