/*A B C D
 *# # # #
 *A B C D
 *# # # #
 */
package Assignment_1;

class PatternCode {
	public static void main(String[] args) {
		
		for(int i = 1;i<=4;i++) {
			char ch = 'A';
			for(int j = 1;j<=4;j++) {
				if(i%2==0) {
					System.out.print("# ");
					
				}else {
					System.out.print(ch+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}

}
