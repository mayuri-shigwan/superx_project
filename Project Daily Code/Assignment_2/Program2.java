/*1 
 *1 2
 *2 3 4
 *4 5 6 7
 * 
 */
package Assignment_3;

class NumPattern {
    public static void main(String[] args){
      int num=1;
      for(int i = 1;i<=4;i++) {
    	  for(int j = 1;j<=i;j++) {
    		  System.out.print(num+" ");
    		  num++;
    	  }
    	  num--;
    	  System.out.println();
    	  
      }
    }
}
