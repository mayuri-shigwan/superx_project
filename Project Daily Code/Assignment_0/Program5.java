/*Reverse numbers and put successive number sum into array and print it
 * 
 */
package SimpleCode;

class ArrayDemo {
	public static void main(String[] args) {
		int num = 45689;
        int n1 = num;
        int count = 0;
        int rev= 0;
        int sum = 0;
        while(n1 != 0){
            count++;
            n1 = n1/10;
        }
        int i = 0;
        int arr[] = new int[count];
        while(num != 0){
            int rem = num%10;
            sum = sum +rem;
            num = num/10;
            rev++;
            if(rev == 2){
                arr[i] = sum;
                rev = 0;
                sum = 0;
                i++;
                num=num*10+rem;
            }
            if(num == 0){
                arr[i] = rem;
            }
        }
        
        for(int j = 0;j<arr.length;j++){
            System.out.print(arr[j] + " ");
        }
	}
}
