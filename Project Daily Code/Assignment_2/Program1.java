/*1 2 3 4
 *2 3 4 5
 *3 4 5 6  
 *4 5 6 7
 */
package Assignment_3;
class PatternCode {
	public static void main(String[] args) {
		int num=4;
		for(int i=1;i<=num;i++) {
			for(int j = 1;j<=num;j++) {
				System.out.print((i+j-1)+" ");
			}
			System.out.println();
		}
	}

}
