/* WAP to print the sum of digits in a given range 1-10
 */
package Assignment_1;
class SumOfDigit {
	public static void main(String[] args) {
		int num = 123456789;
		int sum = 0;
		for(sum=0; num!=0; num=num/10) {
			sum = sum+num%10;
		}
		System.out.println(sum);
		
	}

}
