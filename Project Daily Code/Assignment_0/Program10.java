/*WAP to check whether the given string is palindrome or not.
Input 1 : malayalam
Output 1 : It is palindrome String
Input 2 : armstrong
Output 2 : It is not a palindrome String
Input 3: nayam
 * 
 */
package SimpleCode;
class PalDemo {
	public static void main(String[] args) {
		 String str1 = "nayam";
	        char ch[] =str1.toCharArray();
	        int n = ch.length;
	        char ch1[] =new char[ch.length];
	        for(int i = 0;i<ch.length;i++){
	            ch1[n-i-1] = ch[i];
	        }
	        StringBuilder sb = new StringBuilder();
	        sb.append(ch1);
	        String str2 = new String(sb);
	        System.out.println(str2);
	        if(str1.equals(str2)){
	            System.out.println("It is a Palindrome!!");
	        }else{
	            System.out.println("It is not Palindrome!!");
	        }
	            

	}

}
