/*WAP to print the odd number in the given range 1-10
 * 
 */
package Assignment_3;

class Range{  
	public static void main(String args[]){  
		int num=10;   
		for (int i=1; i<=num; i++){  
			if (i%2!=0){  
				System.out.print(i + " ");  
			}  
		}  
	}  
}  
