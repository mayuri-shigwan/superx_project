
/* WAP to toggle the string to upper case or lower case
 */
package Assignment_1;

class UpLow {
	public static void main(String[] args) {
		char str[] = "Welcome To Core2web".toCharArray();
		System.out.println(String.valueOf(str));
		 for (int i=0; i<str.length; i++){
		        if (Character.isAlphabetic(str[i])) {
		            str[i] = (char)(str[i]^(1<<5));
		        }
		 }
		 System.out.println(String.valueOf(str));
	}

}
