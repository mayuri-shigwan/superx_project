/*WAP to check whether the given no is odd or even
 * 
 */
package Assignment_3;

import java.util.*;

class EvenOddNum {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int num = sc.nextInt();

        if(num % 2 == 0) {
            System.out.println(num + " Number is even");
        }else {
            System.out.println(num + " Numberis odd");
        }
    }
}
